package guiDesign;

import java.io.File;

import javafx.application.Application;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.HPos;
import javafx.geometry.VPos;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane; //THIS allows to add several elements to scene in different positions.
//import javafx.scene.layout.StackPane; //THIS allows to add only one element to scene.
import javafx.stage.Stage;
import javafx.scene.input.Clipboard;
import javafx.scene.input.ClipboardContent;
import java.util.Random;


public class BasicWindow extends Application implements EventHandler<ActionEvent>{
	
	public static void main(String[] args) {
		launch(args);
	}
	
	private static final int LIMIT = 20; //Limit for the generated string - 20 symbols.
	Button button;
	TextField endValueOutput = new TextField();
	Label answer = new Label(); //Label to show the calculated answer.
	
	
	@Override
	public void start(Stage primaryStage) throws Exception{
		Button copyBtn;
		
		primaryStage.setTitle("20-digit Generator v5"); //The window.
		primaryStage.setResizable(false); //Do not allow to change size of the stage/window.
		
		//GridPane with 10px padding around edge
        GridPane grid = new GridPane();
        grid.setPadding(new Insets(10, 10, 10, 10));
        grid.setVgap(8);
        grid.setHgap(10);
        
		
        //Button to generate 20-digit number.
		button = new Button();
		button.setText("Generate");
		button.setOnAction(this); //'this' means, when user clicks the button the code to handle this is in this class.
								//You can make another class instead of 'this'.
		GridPane.setConstraints(button, 0, 0);
		//GridPane.setConstraints(button, 0, 0, 1, 1, HPos.LEFT, VPos.BOTTOM); //If you want the button be at the bottom of its grid.
		
		
		//Name Label - constrains use (child, column, row)
		Label nameLabel = new Label("20-digit numba':");
		GridPane.setConstraints(nameLabel, 0, 1);
		
		//Text field to show generated 20-digit number.
		GridPane.setConstraints(endValueOutput, 0, 2);
		//endValueOutput.setDisable(true); //It does not allow to type into the text field.
										//But the string is barely visible then.
		
		
		
		//Variables for copying function.
		final Clipboard clipboard = Clipboard.getSystemClipboard();
		final ClipboardContent content = new ClipboardContent();
		
		//Button to copy generated 20-digit number.
		copyBtn = new Button();
		copyBtn.setText("Copy");
		copyBtn.setOnAction(e -> {
									content.putString(endValueOutput.getText());
									clipboard.setContent(content);
								}
							); //Lambda usage for button is Awesome!
		GridPane.setConstraints(copyBtn, 1, 2);
		
		
		//BEGIN - Calculation of endValue to equal 16.
		Button buyAnswer = new Button(); //Button for showing the answer.
		
		//BEGIN - Evaluate limit of the string.
		endValueOutput.lengthProperty().addListener(new ChangeListener<Number>(){
			
            public void changed(ObservableValue<? extends Number> observable,
                    Number oldValue, Number newValue) {
                if ( (newValue.intValue() > oldValue.intValue()) || (newValue.intValue() < oldValue.intValue()) ) {
                    // Check if the new character is greater or smaller than LIMIT
                    if ( (endValueOutput.getText().length() >= LIMIT)) {
                    	//Check, if "Generate" button is disabled,
                    	if(buyAnswer.isDisabled()){
                    		buyAnswer.setDisable(false); //then enable it.
                    	}

                        // if it's 11th character then just setText to previous
                        // one
                    	endValueOutput.setText(endValueOutput.getText().substring(0, LIMIT));
                    }else{
                    	buyAnswer.setDisable(true); //Disable "Generate" button, if string is less than 20-digit.
                    	answer.setText(""); //Empty label for answer.
                    }
                }
            }
		});
		//END - Evaluate limit of the string.
		
		buyAnswer.setText("Get answer");
		buyAnswer.setOnAction(e -> {
									try{
										String[] generatedString = endValueOutput.getText().split(""); //Splitting generated 20-digit number into array string-type.
										
										//Element to show answer.
										answer.setText("Amount of '16' value is " + calculationOfNumber.getGeneratedNumber(generatedString));
										GridPane.setConstraints(answer, 0, 6);
									}catch (NumberFormatException error){
										answer.setText("Generate a numba'!");
										GridPane.setConstraints(answer, 0, 6);
									}
									}
							);
		GridPane.setConstraints(buyAnswer, 0, 5);
		//END - Calculation of endValue to equal 16.
		
		
		//Button for showing the task.
		Button taskBtn = new Button("Task?");
		taskBtn.setOnAction(e -> ExplanationOfTask.Explanation());
		GridPane.setConstraints(taskBtn, 3, 0, 1, 1, HPos.RIGHT, VPos.TOP); //columnSpan/rowSpan - the number of columns/rows the child should span.
		
		
		//Author label
		Label authorLabel = new Label("Alek5");
		GridPane.setConstraints(authorLabel, 3, 6, 1, 1, HPos.RIGHT, VPos.BOTTOM);
		
		
		//FUTURE: IMPLEMENT BACKGROUND IMAGE. -- IT SEEMS NOT POSSIBLE WITH 'Grid'.
		//create the image to be used!
	    File image = new File("C:/Users/Aleksandr.Govorkov/workspace_python/JavaFX/src/guiExperiments/samplePic.jpg");
	    String imageFile = image.toURI().toURL().toString();
	    
	    //set some custom properties and add an image
	    ImageView imageView = new ImageView(imageFile);
	    imageView.setFitHeight(100);
	    imageView.setFitWidth(100);
	    //GridPane.setConstraints(imageView, 0, 0); //Optional.
	    grid.getChildren().add(imageView);
		
		
		//FUTURE: EXPORT TO ANDROID WITH BUTTON TO BUY ANSWER.
		
		
		//Add everything to grid
		grid.getChildren().addAll(button, nameLabel, endValueOutput, copyBtn, taskBtn, authorLabel, buyAnswer, answer);
		Scene scene = new Scene(grid, 280, 235); //Body of the window.
		primaryStage.setScene(scene);
		primaryStage.show();
	}

	
	@Override
	public void handle(ActionEvent event) {
		if(event.getSource() == button){ //which button/object caused the code to occur.
			String endValueString = ""; //Variable for full end string.
			
			
			/* THIS PIECE DO NOT GENERATE 0 (ZERO) IN THE BEGINNING.
			//Random randomNumberGenerator = new Random();
			Integer minValue = 1000000000;
			Integer maxValue = 2000000000;
			
			for(int i = 0; i < 2; i++){
				//In order to get a specific range of values first,
				//you need to multiply by the magnitude of the range of values you want covered.
				//Integer magnitudeOfRange = (int) (Math.random() * (maxValue - minValue)); //For debugging.
				//System.out.println(magnitudeOfRange); //For debugging.
				
				Integer random10NumberString = minValue + ((int) (Math.random() * (maxValue - minValue) + 1));
				//System.out.println(random10NumberString); //For debugging.
				
				endValue += Integer.toString(random10NumberString);
				//System.out.println(endValue); //For debugging.
			}
			*/
			
			
			Integer maxDigit = 10; //Further nextInt() excludes value 10;
			Integer maxStringLength = 20; //20 digits should be generated totally.
			
			for(int i = 1; i <= maxStringLength; i++){
				Random randomNumber = new Random();				
				endValueString += Integer.toString(randomNumber.nextInt(maxDigit));
				System.out.println(endValueString); //For debugging.
			}
			
			endValueOutput.setText(endValueString);
			answer.setText(""); //Empty label for answer.
			
		}
	}
	
	

}
