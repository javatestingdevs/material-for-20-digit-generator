package guiDesign;

import java.util.ArrayList;

public class calculationOfNumber {
	/*public static void main(String[] args){
		System.out.println("Amount of value '16' is " + getGeneratedNumber());
	}
	*/
	
	public static Integer getGeneratedNumber(String[] processinbgString){
		Integer count = 0;
		
		ArrayList<Integer> myArray = new ArrayList<Integer>();
		for(String s : processinbgString) myArray.add(Integer.parseInt(s));
		
		System.out.println(myArray); //For debugging.
		
		try{
			for(int i=0; i <= myArray.size()-1; i++){
				int sum = (int) myArray.toArray()[i];
				
				int j=i;
				while(sum <= 16){
					if(j != myArray.size()-1){
						j++;
						
						sum += (int) myArray.toArray()[j];
						if(sum == 16){
							count++;
						}
					}else{
						break;
					}
				}
			}
		}catch (ArrayIndexOutOfBoundsException e){
			System.out.println("THE END"); //For debugging.
			
		}
		
		return count;
	}

}
