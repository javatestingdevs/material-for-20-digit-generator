package guiDesign;

import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;

public class ExplanationOfTask {
	
	public static void Explanation(){
		Stage window = new Stage();
		window.setResizable(false);
		
		window.initModality(Modality.APPLICATION_MODAL); //Defines a modal window that blocks events from being delivered to any other application window.
		window.setTitle("The Task is to");
		
		//Description of the task.
		Label label1 = new Label();
		label1.setText("find sequence of two or more digits,");
		
		//Description of the task.
		Label label2 = new Label();
		label2.setText("which give sum of 16.");
		
		Button closeBtn = new Button("Dismiss");
		closeBtn.setOnAction(e -> window.close());
		
		VBox layout = new VBox(10);
		layout.getChildren().addAll(label1, label2, closeBtn);
		layout.setAlignment(Pos.CENTER);
		
		Scene scene = new Scene(layout);
		window.setScene(scene);
		window.showAndWait();
	}
}
